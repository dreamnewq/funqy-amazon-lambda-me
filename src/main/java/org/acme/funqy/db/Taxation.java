package org.acme.funqy.db;

public record Taxation(
        Long id,
        String name,
        String description
){}

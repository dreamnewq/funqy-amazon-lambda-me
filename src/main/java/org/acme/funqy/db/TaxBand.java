package org.acme.funqy.db;

import org.acme.funqy.db.Taxation;

import java.math.BigDecimal;

public record TaxBand(
        Long id,
        BigDecimal lowerLimit,
        BigDecimal upperLimit,
        BigDecimal percent,
        Taxation taxation
){}

package org.acme.funqy.db;

import jakarta.enterprise.context.ApplicationScoped;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class TaxDatabaseDataLoader {

    public List<TaxBand> executeQueryAndGetTaxBands(String sqlQuery, Map<String, String> secretsMap) {
        List<TaxBand> taxBands = new ArrayList<>();

        try (Connection connection = getDbConnection(secretsMap);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sqlQuery)) {
            while (resultSet.next()) {
                TaxBand taxBand = fromRow(resultSet);
                taxBands.add(taxBand);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taxBands;
    }

    public List<TaxBand> loadTaxBands(String taxationName, Map<String, String> dbCredentials) {
        List<TaxBand> taxBands = new ArrayList<>();

        String sqlQuery = constructTaxBandQuery(taxationName);

        try (Connection connection = getDbConnection(dbCredentials);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sqlQuery)) {
            while (resultSet.next()) {
                TaxBand taxBand = fromRow(resultSet);
                taxBands.add(taxBand);
            }
        } catch (Exception e) {
            // Proper error handling
        }
        return taxBands;
    }



    private static TaxBand fromRow(ResultSet resultSet) throws Exception {
        return new TaxBand(
                resultSet.getLong("id"),
                resultSet.getBigDecimal("lower_limit"),
                resultSet.getBigDecimal("upper_limit"),
                resultSet.getBigDecimal("percent"),
                new Taxation(resultSet.getLong("taxation_id"), resultSet.getString("name"), null)

        );
    }


    private static Connection getDbConnection(Map<String, String> secretsMap) throws SQLException {
        String host =       secretsMap.get("host");
        String dbname =     secretsMap.get("dbname");
        String username =   secretsMap.get("username");
        String password =   secretsMap.get("password");
        int port =          Integer.parseInt(secretsMap.get("port"));

        String connectionUrl = "jdbc:mysql://" + host + ":" + port + "/" + dbname + "?useSSL=false";
        return DriverManager.getConnection(connectionUrl, username, password);
    }

    private static String constructTaxBandQuery(String taxationName) {
        return String.format(
                "SELECT tax_band.*, taxation.name FROM tax_band JOIN taxation ON tax_band.taxation_id = taxation.id WHERE taxation.name = '%s'",
                taxationName
        );
    }


}

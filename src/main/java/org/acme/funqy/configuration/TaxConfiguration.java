package org.acme.funqy.configuration;

import jakarta.enterprise.context.ApplicationScoped;
import org.acme.funqy.db.TaxBand;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.List;
import java.util.Map;

@ApplicationScoped
public class TaxConfiguration {

    private Map<String, String> dbCredentials;
    private List<TaxBand> taxBands;

    @ConfigProperty(name = "taxation.name")
    private String taxationName;


    public Map<String, String> getDbCredentials() {
        return dbCredentials;
    }

    public void setDbCredentials(Map<String, String> dbCredentials) {
        this.dbCredentials = dbCredentials;
    }

    public List<TaxBand> getTaxBands() {
        return taxBands;
    }

    public void setTaxBands(List<TaxBand> taxBands) {
        this.taxBands = taxBands;
    }

    public String getTaxationName() {
        return taxationName;
    }
}

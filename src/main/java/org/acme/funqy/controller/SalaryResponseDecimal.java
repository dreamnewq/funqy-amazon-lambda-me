package org.acme.funqy.controller;

import java.math.BigDecimal;

public record SalaryResponseDecimal(
        BigDecimal grossAnnualSalary,
        BigDecimal grossMonthlySalary,
        BigDecimal netAnnualSalary,
        BigDecimal netMonthlySalary,
        BigDecimal annualTax,
        BigDecimal monthlyTax
) { }

package org.acme.funqy.controller;

public record SalaryRequest(double grossAnnualSalary) {
}


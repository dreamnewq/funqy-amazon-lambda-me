package org.acme.funqy.init;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.acme.funqy.configuration.TaxConfiguration;
import org.acme.funqy.db.TaxBand;
import org.acme.funqy.db.TaxDatabaseDataLoader;
import org.acme.funqy.SecretManagerService;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.List;
import java.util.Map;

@ApplicationScoped
public class ApplicationInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationInitializer.class);


    @Inject
    SecretManagerService secretManagerService;

    @Inject
    TaxDatabaseDataLoader taxDatabaseDataLoader;

    @Inject
    TaxConfiguration taxConfiguration;

    @ConfigProperty(name = "aws.secretmanager.dbcredentials-secret")
    String dbSecretName;

    @ConfigProperty(name = "taxation.name")
    String taxationName;

    @PostConstruct
    public void init() {
        LOGGER.info("Initializing application data...LOGGER");
        Map<String, String> dbCredentials = secretManagerService.parseAllKeyValuesFromSecret(dbSecretName);
        LOGGER.info("Loaded DB credentials: {}", dbCredentials);
        List<TaxBand> taxBands = taxDatabaseDataLoader.loadTaxBands(taxationName, dbCredentials);
        LOGGER.info("Loaded tax bands: {}", taxBands);
        taxConfiguration.setDbCredentials(dbCredentials);
        taxConfiguration.setTaxBands(taxBands);
    }
}
